//
// File: queries.js
// Auth: Martin Burolla
// Date: 9/20/2022
// Desc: Contains all the write actions for the GraphQL API.
//

const userData = require('../MOCK_DATA.json')
const UserType = require('./TypeDefs/UserType')
const { 
    GraphQLInt, 
    GraphQLString,
    GraphQLObjectType, 
} = require("graphql")

const Mutations = new GraphQLObjectType({
    name: "Mutations",
    fields: {
        createUser: { // <== Similar to a POST web API endpoint.
            type: UserType,
            args: {
                email: { type: GraphQLString },
                gender: { type: GraphQLString },
                last_name: { type: GraphQLString },
                ip_address: { type: GraphQLString },
                first_name: { type: GraphQLString }
            },
            resolve(parent, args) {
                const newUser = {
                    email: args.email,
                    gender: args.gender,
                    id: userData.length + 1, 
                    last_name: args.last_name,
                    ip_address: args.ip_address,
                    first_name: args.first_name
                }
                userData.push(newUser)
                return newUser
            }
        },
        updateIpAddress: { // <== Similar to a PUT/PATCH web API endpoint.
            type: UserType,
            args: {
                id: { type: GraphQLInt },
                ip_address: { type: GraphQLString }
            },
            resolve(parent, args) {
                let user = userData[args.id]
                user.ip_address = args.ip_address
                return userData[args.id]
            }
        },
        deleteUser: { // <== Similar to a DEL web API endpoint.
            type: GraphQLString,
            args: {
                id: { type: GraphQLInt },
            },
            resolve(parent, args) {
                userData.splice(args.id-1, 1)
                return `Deleted user id: ${args.id}`
            }
        }

        //
        // TODO: Add more mutations here...
        //
    }
})

module.exports = Mutations
